﻿$(document).ready(function () {
    
    $('.menu-trigger').click(function () {
        $('#header-menu').slideToggle(400, function () {
            $(this).toggleClass('expand-menu');
        });
    });

    $('.footer-title').click(function () {
        $(this).next().slideToggle();
    });
});