﻿jQuery(document).ready(function () {

    $('#header, #banner, #bienvenida-span p').animate({ 'opacity': '1' }, 1000);

    if (jQuery(document).width() > 1024) {

        jQuery(".appear").appear();
        jQuery(".appear").on('appear',
            function () {
                var effect;
                if (jQuery(this).attr('animation-class') !== undefined && jQuery(this).attr('animation-delay-time') !== undefined) {

                    if (!jQuery(this).hasClass('animated')) {
                        effect = 'animated ' + jQuery(this).attr('animation-class');
                        var time = jQuery(this).attr('animation-delay-time');
                        var element = this;
                        setTimeout(function () {
                            jQuery(element).addClass(effect);
                        }, time);
                    }
                } else if (jQuery(this).attr('animation-class')) {
                    if (!jQuery(this).hasClass('animated')) {
                        effect = 'animated ' + jQuery(this).attr('animation-class');
                        jQuery(this).addClass(effect);
                    }
                } else {
                    if (!jQuery(this).hasClass('animated')) {
                        jQuery(this).addClass("animated fadeIn");
                    }
                }
            }
        );

    }

    $('.eventos-thumbs').hover(function () {
        $(this).animate({
            "opacity": "-=0.3"
        }, 100);
    }, function () {
        $(this).animate({
            "opacity": "+=0.3"
        }, 100);
    });

});