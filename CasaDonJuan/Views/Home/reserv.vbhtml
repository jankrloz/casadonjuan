﻿@Code
    ViewData("Title") = "Reservaciones | Casa de Don Juan Manuel"
End Code

@Section othercss
    @Styles.Render("~/Content/css/reserv")
End Section

<!-- Aquí va el banner-->
<div>
    <img id="banner" src="~/Images/Home/banner.jpg" alt="" />
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-xs-12">
                    <h2>RESERVACIONES</h2>
                    <p id="reserv-text">
                        Al solicitar su reservación le enviaremos una cotización y los datos completos para depositar su anticipo. . .
                        <br /><br />
                        Le suplicamos lea detenidamente el Reglamento que deberá de firmar al contratar LA CASA DE DON JUAN MANUEL
                        <br /><br />
                        Descargar Reglamento <a href="#"><span>Aquí</span> <i class="fa fa-file-o"></i></a>
                    </p>
                    <img src="~/Images/Reservaciones/img_secc.jpg" alt="reservaciones" />
                </div>
                <div class="col-xs-12"></div>
            </div>
        </div>
        <div class="col-lg-6">
            <form>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="text" name="name" value="" placeholder="Nombre" />

                    </div>
                    <div class="col-sm-6">
                        <input type="email" name="email" value="" placeholder="Correo Electrónico" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="email" name="confirm-email" value="" placeholder="Confirmar Correo Electrónico" />

                    </div>
                    <div class="col-sm-6">
                        <input type="tel" name="tel" value="" placeholder="Teléfono" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="fecha">Fecha del Evento</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" id="datepicker">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="fecha">Tipo del Evento</label>
                    </div>
                    <div class="col-sm-9">
                        <input list="tipo-evento">

                        <datalist id="tipo-evento">
                            <option value="Tipo-1">
                            <option value="Tipo-2">
                            <option value="Tipo-3">
                            <option value="Tipo-4">
                            <option value="Tipo-5">
                        </datalist>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label id="label-comentarios" for="comentarios">Comentarios</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <textarea id="comentarios" name="comentarios"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="btn-1">
                            <span>Enviar</span>
                        </div>
                        <div id="btn-2">
                            <i class="fa fa-angle-right"></i>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="~/Scripts/moment.js"></script>
<script src="~/Scripts/pikaday.js"></script>
<script>
    var picker = new Pikaday({
        field: document.getElementById('datepicker'),
        format: 'L',
        onSelect: function () {
            console.log(this.getMoment().format('Do MMMM YYYY'));
        }
    });
</script>