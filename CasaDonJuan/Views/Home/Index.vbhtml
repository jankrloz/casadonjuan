﻿@Code
    ViewData("Title") = "Casa de Don Juan Manuel"
End Code

@Section othercss
    @Styles.Render("~/Content/css/index")
End Section

<!-- Aquí va el banner-->
<div>
    <img id="banner" src="~/Images/Home/banner.jpg" alt="" />
</div>

<div id="bienvenida" class="container">
    <div class="row">
        <div id="bienvenida-span" class="col-md-12">
            <p>
                En <span>Casa de Don Juan Manuel</span> encontrará
                una<br />Sede llena de <span>Tradición</span> y <span>Buen Gusto</span>
                para su evento
            </p>
        </div>
    </div>
    <hr class="line" />
    <div class="appear alpha row" animation-class="fadeIn" animation-delay-time="200">
        <div class="col-sm-6">
            <p id="bienvenida-title">BIENVENIDA</p>
            <div id="bienvenida-text">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis tristique felis, ut aliquam velit. Donec eget leo faucibus, varius quam a, facilisis tellus. Mauris imperdiet erat enim, quis posuere quam vulputate vel. In ex lorem, varius a lacus blandit, consectetur blandit leo. Donec lobortis felis urna, at ullamcorper nulla cursus ut. Integer vitae nunc vel ipsum ultricies posuere a vitae neque. Vestibulum sagittis ultrices ex. Proin vitae pretium diam. Nullam posuere lectus sed erat tempor, ac vulputate dolor dictum.
                </p>
                <p>
                    Phasellus aliquam pulvinar nunc a pellentesque. Quisque ac nibh id diam fermentum porta. Phasellus sed tempus ligula. Morbi varius venenatis nisi nec dictum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Ut imperdiet porta venenatis. Suspendisse non mauris lacus. Nullam tempus placerat volutpat. Nulla et ante aliquam, vehicula est id, ornare nisi.
                </p>
            </div>
        </div>
        <div class="col-sm-6">
            <center><img id="img-bienvenida" src="~/Images/Home/img_bienvenida.jpg" alt="" /></center>
        </div>
    </div>
</div>

<div id="eventos-div" class="appear alpha" animation-class="fadeIn" animation-delay-time="200">
    <div class="container">
        <div class="row">
            <div class="col-xs-3 col-sm-2 col-md-1">
                <span id="eventos-title">Eventos</span>
            </div>
            <div class="col-xs-9 col-sm-10 col-md-11">
                <hr /><hr />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-15">
                <div class="eventos-thumbs">
                    <center><img src="~/Images/Home/Eventos/fiestas.jpg" alt="" /></center>
                    <div class="eventos-thumbs-titles"><span>Fiestas</span></div>
                </div>
            </div>
            <div class="col-xs-15">
                <div class="eventos-thumbs">
                    <center><img src="~/Images/Home/Eventos/bodas.jpg" alt="" /></center>
                    <div class="eventos-thumbs-titles"><span>Bodas</span></div>
                </div>
            </div>
            <div class="col-xs-15">
                <div class="eventos-thumbs">
                    <center><img src="~/Images/Home/Eventos/eventos.jpg" alt="" /></center>
                    <div class="eventos-thumbs-titles"><span>Eventos</span></div>
                </div>
            </div>
            <div class="col-xs-15">
                <div class="eventos-thumbs">
                    <center><img src="~/Images/Home/Eventos/xvanos.jpg" alt="" /></center>
                    <div class="eventos-thumbs-titles"><span>XV Años</span></div>
                </div>
            </div>
            <div class="col-xs-15">
                <div class="eventos-thumbs">
                    <center><img src="~/Images/Home/Eventos/bautizo.jpg" alt="" /></center>
                    <div class="eventos-thumbs-titles"><span>Bautizos</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="appear alpha row" animation-class="fadeIn" animation-delay-time="200">
        <div id="servicios" class="thumb col-xs-4">
            <div class="thumb-container">
                <i class="fa fa-check-circle"></i>
                <p>Servicios</p>
                <p>Contamos con gran variedad de servicios para hacer de su evento algo inolvidable.</p>
                <p>Ver más</p>
            </div>
        </div>
        <div id="reservaciones" class="thumb col-xs-4">
            <div class="thumb-container">
                <i class="fa fa-calendar"></i>
                <p>Reservaciones</p>
                <p>No olvides consultar nuestro calendario de eventos y reservar la fecha para el tuyo.</p>
                <p>Ver más</p>
            </div>
        </div>
        <div id="ubicacion" class="thumb col-xs-4">
            <div class="thumb-container">
                <i class="fa fa-thumb-tack"></i>
                <p>Ubicación</p>
                <p>Ubicados en el corazón de la Ciudad, le ofrecemos un lugar mágico y único.</p>
                <p>Ver más</p>
            </div>
        </div>
    </div>
</div>

<div id="mapa" class="appear alpha" animation-class="fadeIn" animation-delay-time="200">
    <img src="~/Images/Home/mapa.jpg" alt="" />
</div>

<div id="coment-banq" class="container">
    <div class="row appear alpha" animation-class="fadeIn" animation-delay-time="200">
        <div class="col-lg-7">
            <div class="row">
                <div class="col-sm-3">
                    <span id="coment-title">COMENTARIOS</span>
                </div>
                <div class="col-sm-9">
                    <hr /><hr />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 coment-div">
                    <center><img src="~/Images/Home/Comentarios/01.jpg" alt="" /></center>
                    <p class="coment-text">
                        “Todo estuvo perfecto, el lugar muy bonito y el
                        servicio de primera, fué el día más especial de mi vida y
                        todo salió perfecto gracias Casa Don Juan Manuel.”
                    </p>
                    <p class="coment-name">Marina</p>
                </div>
                <div class="col-xs-4 coment-div">
                    <center><img src="~/Images/Home/Comentarios/02.jpg" alt="" /></center>
                    <p class="coment-text">
                        “El servicio estuvo de lujo fué un día muy especial y
                        todo salió muy bien gracias Casa Don Juan Manuel.”
                    </p>
                    <p class="coment-name">Claudia</p>
                </div>
                <div class="col-xs-4 coment-div">
                    <center><img src="~/Images/Home/Comentarios/03.jpg" alt="" /></center>
                    <p class="coment-text">
                        “Mi esposo estuvo muy feliz en su cumpleaños, todo
                        salió tal cual nos lo dijeron, gracias Casa Don Juan Manuel.”
                    </p>
                    <p class="coment-name">Ana Luisa</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-lg-offset-1">
            <div class="row">
                <div class="col-sm-4">
                    <span id="banq-title">BANQUETES</span>
                </div>
                <div class="col-sm-8">
                    <hr /><hr />
                </div>
            </div>
        </div>
    </div>
</div>