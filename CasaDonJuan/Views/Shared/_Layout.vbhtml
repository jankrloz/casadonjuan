﻿<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title</title>
    @Styles.Render("~/Content/css")
    @Styles.Render("~/Content/css/effects")
    @Scripts.Render("~/bundles/modernizr")
    @RenderSection("othercss", required:=False)
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="~/Content/animate.css" rel="stylesheet">
    <script src="http://use.edgefonts.net/source-sans-pro.js"></script>
</head>
<body>

    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=114065665271745&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div id="header-top-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span>
                        Teléfono de Atención: 01 (55) 5597 - 2268
                    </span>
                    <i class="fa fa-envelope-o"></i>
                    <i class="fa fa-comments"></i>
                </div>
            </div>
        </div>
    </div>

    <div id="header" class="container">
        <div class="row">
            <div id="header-logo" class="col-lg-4">
                <center><img src="~/Images/Home/logo.jpg" alt="" /></center>
            </div>
            <div class="menu-trigger col-xs-12">
                <span>MENU</span>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bars"></i>
            </div>
            <div id="header-menu" class="col-lg-8">
                <ul>
                    <li><a href="casa-don-juan-manuel.html">Inicio</a></li>
                    <li><a href="">Servicios</a></li>
                    <li><a href="~/reservaciones.html">Reservaciones</a></li>
                    <li><a href="">Galería</a></li>
                    <li><a href="">Ubicación</a></li>
                </ul>
            </div>
        </div>
    </div>

    @RenderBody()

    <footer>
        <div class="container">
            <div class="row">
                <div id="footer-1" class="col-sm-4">
                    <p class="footer-title">CASA DE DON JUAN MANUEL</p>
                    <p class="footer-content">
                        En Casa de Don Juan Manuel, su belleza y su
                        leyenda son reconocidas y está catalogada
                        como Monumento Histórico por el Instituto
                        Nacional de Antropología e Historia INAH y es
                        un orgullo del Centro Histórico de la ciudad de
                        México.<br>
                        Su conservación y cuidado convierten en un
                        privilegio el obtener el uso del inmueble para
                        la celebración de su Evento.
                    </p>
                </div>
                <div id="footer-2" class="col-sm-2">
                    <p class="footer-title">NUESTRA EMPRESA</p>
                    <ul class="footer-content">
                        <li><a href="">Quiénes Somos</a></li>
                        <li><a href="">Servicios</a></li>
                        <li><a href="~/reservaciones.html">Reservaciones</a></li>
                        <li><a href="">Ubicación</a></li>
                        <li><a href="">Galería</a></li>
                    </ul>
                </div>
                <div id="footer-3" class="col-sm-3">
                    <p class="footer-title">OFICINA CORPORATIVA</p>
                    <p class="footer-content">
                        República de Uruguay #90<br />
                        Col. Centro, Deleg. Cuauhtémoc<br />
                        México D.F.
                        <br /><br />
                        Tel: 01 (55) 5597 - 2268
                    </p>
                </div>
                <div id="footer-4" class="col-sm-3">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="footer-title">COMPARTE</p>
                            <div class="circle"><center><i class="fa fa-facebook"></i></center></div>
                            <div class="circle"><center><i class="fa fa-twitter"></i></center></div>
                            <div class="circle"><center><i class="fa fa-google-plus"></i></center></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="footer-title">SÍGUENOS</p>
                            <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="footer-title">VISÍTANOS</p>
                            <div class="circle"><center><i class="fa fa-facebook"></i></center></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p id="footer-privacy">Aviso de Privacidad</p>
                    <p id="footer-company-name">
                        Casa Don Juan Manuel S.A. de C.V. © 2014 &nbsp;&nbsp;&nbsp;&nbsp; Diseño y Posicionamiento Web Por <a href="http://www.adwebsys.com">Adwebsys</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/adwebsys")
    @Scripts.Render("~/bundles/effects")

    <script src="~/Scripts/jquery.appear.js"></script>

    @RenderSection("scripts", required:=False)


</body>
</html>
